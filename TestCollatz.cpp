// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

TEST(CollatzFixture, read2) {
    istringstream         iss("-5 -2\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  -5);
    ASSERT_EQ(j, -2);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(100, 200));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(201, 210));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(900, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(90347, 724484));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  90347);
    ASSERT_EQ(j, 724484);
    ASSERT_EQ(v, 509);
}

TEST(CollatzFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(435, 831));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  435);
    ASSERT_EQ(j, 831);
    ASSERT_EQ(v, 171);
}

TEST(CollatzFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(874, 981));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  874);
    ASSERT_EQ(j, 981);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval7) {
    tuple<int, int, int> t = collatz_eval(make_pair(70446, 434950));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  70446);
    ASSERT_EQ(j, 434950);
    ASSERT_EQ(v, 449);
}

TEST(CollatzFixture, eval8) {
    tuple<int, int, int> t = collatz_eval(make_pair(1034, 586194));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1034);
    ASSERT_EQ(j, 586194);
    ASSERT_EQ(v, 470);
}

TEST(CollatzFixture, eval9) {
    tuple<int, int, int> t = collatz_eval(make_pair(2905, 344819));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  2905);
    ASSERT_EQ(j, 344819);
    ASSERT_EQ(v, 443);
}

TEST(CollatzFixture, eval10) {
    tuple<int, int, int> t = collatz_eval(make_pair(35536, 176316));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  35536);
    ASSERT_EQ(j, 176316);
    ASSERT_EQ(v, 383);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

TEST(CollatzFixture, print2) {
    ostringstream oss;
    collatz_print(oss, make_tuple(874, 981, 174));
    ASSERT_EQ(oss.str(), "874 981 174\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}
