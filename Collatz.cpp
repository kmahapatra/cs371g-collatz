// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <map>      // map
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <vector>   // vector

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (istream_iterator<int>& begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------


enum { kMaxInput = 1000000 };
enum { kCacheRange = 1000 };

// `cycle_length` is a map used for "memoization" of collatz length problem.
// Memoization is a mapping to avoid repeating a computation. Here the input
// is the integer for which we are calculating the cycle length. This is used
// by the function `collatz_length`.
static map<long, int> cycle_length;

// return the cycle length for a given number.
static int collatz_length(long n) {
    int length = cycle_length[n];
    if (length == 0) {
        if (n == 1) {
            length = 1;
        } else if (n % 2 == 0) {
            length = 1 + collatz_length(n / 2);
        } else {
            // We want to use a lower index to benefit from cache.
            // 3 * n + 1 is an even number.
            // So collatz_length(3*n+1) = 1 + collatz_length((3*n+1)/2).
            // (3*n+1)/2 is n + n/2 + 1 when n is odd.
            // So the expression becomes 1 + collatz_length(n + n / 2 + 1).
            length = 2 + collatz_length(n + n / 2 + 1);
        }
        cycle_length[n] = length;
    }
    return length;
}

// `range_max_length` is a vector used for "memoization" of collatz max length between
// two numbers. It divides the problem into multiple ranges where each range is
// `kCacheRange` (1000) long. For example, numbers between 1-1000 go in range 0, 
// numbers between 1001-2000 go in range 1 and so on. Here the input is the range number
// such as 1 and output is the max cycle length for all numbers between 1001-2000 (all
// numbers in range 1). It is used by the function `collatz_eval`.
static vector<int> range_max_length((kMaxInput + kCacheRange - 1) / kCacheRange);

// return the max cycle length for numbers between two given numbers (inclusive)
static int collatz_max_length(int b, int e) {
    assert(b <= e);
    assert(e - b + 1 <= kCacheRange);
    int m = e / 2 + 1;
    assert(m <= e);
    while (b < e) {
        if (b < m) {
            b = m;
        } else {
            break;
        }
    }
    int max_length = 0;
    for (; b <= e; b++) {
        max_length = max(max_length, collatz_length(b));
    }
    return max_length;
}

static int range_index(long n) {
    return (n - 1) / kCacheRange;
}

static int range_left(int range) {
    return range * kCacheRange + 1;
}

static int range_right(int range) {
    return (range + 1) * kCacheRange;
}

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    tie(i, j) = p;
    assert(i > 0);
    assert(i < kMaxInput);
    assert(j > 0);
    assert(j < kMaxInput);
    if (i > j) {
        std::swap(i, j);
    }
    int max_length = 0;
    int range1 = range_index(i);
    int range2 = range_index(j);
    if (i > range_left(range1)) {
        int hi = min(range_right(range1), j);
        max_length = collatz_max_length(i, hi);
        i = hi + 1;
        range1++;
    }
    if (i <= j && j < range_right(range2)) {
        int lo = max(range_left(range2), i);
        max_length = max(max_length, collatz_max_length(lo, j));
        range2--;
    }
    for (; range1 <= range2; range1++) {
        if (range_max_length[range1] == 0) {
            range_max_length[range1] = collatz_max_length(range_left(range1), range_right(range1));
        }
        max_length = max(max_length, range_max_length[range1]);
    }
    return make_tuple(p.first, p.second, max_length);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    while (begin_iterator != end_iterator) {
        collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}
