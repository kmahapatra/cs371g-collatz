# CS371g: Generic Programming Collatz Repo

* Name: Koshik Mahapatra

* EID: km47526

* GitLab ID: kmahapatra

* HackerRank ID: koshikmahapatra

* Git SHA: 5232d44bd72511d262f9cdf4013c227845e07895

* GitLab Pipelines: https://gitlab.com/kmahapatra/cs371g-collatz/-/pipelines

* Estimated completion time: 25

* Actual completion time: 20

* Comments: It passes all the hacker-rank tests.
